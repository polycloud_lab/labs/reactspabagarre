FROM node:12.18.2 as build
WORKDIR /app
COPY . .
RUN npm ci
RUN npm run build


FROM nginx
COPY --from=build /app/build /usr/share/nginx/html