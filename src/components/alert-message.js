export default function AlertMessage({ children }) {
  return <div className="notification is-primary my-5">{children}</div>;
}
